# TODO list for Element V-112

### Todo:
- [ ] Engine
	- [x] Camera
	- [ ] Mechanics
		- [x] Core movements
		- [x] Polish code switcheroo mess
		- [ ] Ladders
		- [ ] Special abilities
			- [x] Superjump
			- [ ] Dash
			- [ ] Crouchroll
	- [ ] Gameplay
		- [ ] Inventory
			- [ ] Items
			- [ ] Weapons
			- [ ] Ammo
		- [ ] Combat
			- [ ] Weapons
			- [ ] Enemies
		- [ ] Triggers
- [ ] Design
	- [x] Collisions
	- [x] Cel shading
	- [ ] Texturing workflow
- [ ] GUI
	- [ ] Main menu
	- [ ] Pause menu
		- [ ] Inventory
	- [ ] Ingame settings
	- [ ] Setting editor
	- [ ] Dev. console
- [ ] Game
	- [ ] Sprites
		- [x] Player
		- [ ] Doc
		- [ ] Natanie
		- [ ] Enemies
	- [ ] 3D models
		- [ ] AA turret
		- [ ] Truck
		- [ ] Tank
	- [ ] Items
		- [ ] Weapons
			- [ ] Handgun
			- [ ] Grenade launcher
			- [ ] Rocket launcher
			- [ ] Jolt gun
		- [ ] Medipak
		- [ ] Keys
	- [ ] Levels
		- [ ] Debug
			- [ ] Mechanics testing
			- [ ] Weapon testing
		- [ ] Level 1 (Military outpost)
			- [ ] Outer base
			- [ ] Control tower
			- [ ] Satellite control center
	- [ ] Achievements
- [ ] Music
	- [ ] Pray & cry ;_;
	

### Bugs:

### Roadmap:
- [ ] Get basic gameplay engine working, test some assets against it
- [ ] Develop testing build
- [ ] Polish github/create dedicated website
- [ ] Develop prototype working game
- [ ] Sell on Steam
- [ ] Develop multiple gameplays
- [ ] Implement Steam Workshop
- [ ] ???
- [ ] Profit