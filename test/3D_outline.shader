shader_type spatial;
render_mode 
blend_mix, depth_draw_opaque,cull_front,diffuse_lambert,specular_schlick_ggx,unshaded;
uniform float grow;


void vertex() {
	VERTEX+=NORMAL*grow;
}

void fragment() {
	ALBEDO = vec3(0.0,0.0,0.0);
	ALPHA = 1.0;
}