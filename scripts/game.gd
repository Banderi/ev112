extends Node

###

const NULL = -1

const WP_PLASMA = 0
const WP_MP8 = 1
const WP_GRENADE = 2
const WP_JOLT = 3

const AM_EXPLOSIVE = 0
const AM_ENERGY = 1

const FS_SEMI = 0
const FS_AUTO = 1

const SK_DASH = 0
const SK_INVULN = 1

###

var ded = false
var fatigue = false

var health = 100
var stamina = 100
var skill = 0

var hue_null = Color(1,1,1,0)
var hue_weapons = Color(0.4,0.4,20,0.75)
var hue_health = Color(20,0.4,0.4,0.75)
var hue = hue_null

###

var switch_on_new = true
var equip_on_pickup = true

###

var weapons = [false,false,false,false]
var melee = true;

var sel_skill = SK_DASH
var sel_weapon = NULL
var sel_lastweapon = NULL
var sel_ammo = AM_EXPLOSIVE
var sel_rof = FS_AUTO
var sel_item = NULL

var amm_mp8 = 0
var amm_grenade = [0, 0]
var amm_energy = 0

var energy_buildup = 0
var energy_jolt = 0

var clp_plasma = 7	# clip is magazine capacity, magazine is current buffer
var clp_mp8 = 12

var mag_plasma = 7
var mag_mp8 = clp_mp8

#var max_health = 100
#var max_stamina = 100
var max_mp8 = 48
var max_grenade = [6,6]
var max_energy = 99

var reloading = 0
var cooldown = 0
var rof_plasma = 12
var rof_mp8 = 2
var rof_grenade = 35
var rel_plasma = 15
var rel_mp8 = 15

func spawn(object):
	pass

func pickup(object):
	var ammoleft = 0;

	if (weapons[object["item"]] == true && object["ammo"] == 0):
		return -1;


	if (object["item"] <= WP_JOLT): # it's a weapon

		if (object["ammo"] > 0):	# weapon has ammo
			var ammopickup = 0;

			if (object["item"] == WP_MP8 && amm_mp8 < max_mp8):
				ammopickup = min(object["ammo"], max_mp8 - amm_mp8);
				amm_mp8 += ammopickup;

			elif (object["item"] == WP_GRENADE && amm_grenade[object["ammotype"]] < max_grenade[object["ammotype"]]):
				ammopickup = min(object["ammo"], max_grenade[object["ammotype"]] - amm_grenade[object["ammotype"]]);
				amm_grenade[object["ammotype"]] += ammopickup;

			elif (object["item"] == WP_JOLT && amm_energy < max_energy):
				ammopickup = min(object["ammo"], max_energy - amm_energy);
				amm_energy += ammopickup;

			ammoleft = object["ammo"] - ammopickup;

		if (weapons[object["item"]] == false): # weapon is new
			weapons[object["item"]] = true;
			hue = hue_weapons;
			if (sel_weapon == NULL || switch_on_new || equip_on_pickup):
				sel_weapon = object["item"];
				if (equip_on_pickup):
					melee = false;

		if (ammoleft != object["ammo"]):
			hue = hue_weapons;

	return ammoleft;


###

func plasma(shots):
	mag_plasma -= shots;
	cooldown = rof_plasma * shots;
	if (mag_plasma == 0):
		reloading = rel_plasma;

func bullet():
	mag_mp8 -= 1;
	cooldown = rof_mp8;
	if (mag_mp8 == 0 && amm_mp8 > 0):
		reloading = rel_mp8;

func grenade(type):
	if (amm_grenade[type] > 0):
		cooldown = rof_grenade;
	amm_grenade[type] -= 1;

func jolt(buildup):
#	damage = base + buildup;
#	buildup = -buildup;
	cooldown = 37 - 37 / (0.2 * energy_buildup + 1)
	energy_buildup = 0;
	energy_jolt += (15 - energy_jolt) * 0.35;

###

func _process(delta):

	if (health <= 0):
		ded = true;
		health = 0;

	if (stamina > 100):
		stamina = 100;
	elif (stamina < -20):
		stamina = -20;

	if (fatigue && stamina > 50):
		fatigue = false;
	elif (!fatigue && stamina < 0):
		fatigue = true;

	if (reloading == 0 && cooldown == 0 && !melee):
		if (sel_weapon == WP_PLASMA):
			if (Input.is_action_pressed("shoot")):
				plasma(min(1,mag_plasma));
			elif (Input.is_action_pressed("secondary_shoot")):
				plasma(min(2,mag_plasma));

		elif (sel_weapon == WP_MP8 && sel_rof == FS_AUTO):
			if (Input.is_action_pressed("shoot")):
				bullet();

		elif (sel_weapon == WP_GRENADE):
			if (Input.is_action_pressed("shoot")):
				grenade(AM_EXPLOSIVE);
			elif (Input.is_action_pressed("secondary_shoot")):
				grenade(AM_ENERGY);

		elif (sel_weapon == WP_JOLT):
			if (Input.is_action_pressed("shoot") && amm_energy > 0):
				amm_energy -= delta * 7.5;
				jolt(energy_buildup);
			elif (Input.is_action_pressed("secondary_shoot") && amm_energy > 0):
				var d = delta * 5;
				amm_energy -= d;
				energy_buildup += d;
			elif (energy_buildup != 0):
				jolt(energy_buildup);
	elif (reloading > 0):
		reloading -= delta * 10;
		if (reloading <= 0):
			reloading = 0;
			if (sel_weapon == WP_PLASMA):
				mag_plasma = clp_plasma;
			elif (sel_weapon == WP_MP8):
				mag_mp8 = min(clp_mp8, amm_mp8);
				amm_mp8 -= min(clp_mp8, amm_mp8);
	cooldown -= delta * 20;
	if (cooldown <= 0):
		cooldown = 0;
	energy_jolt -= delta * 30;
	if (energy_jolt <= 0):
		energy_jolt = 0;

	if (hue != hue_null):
		hue += (hue_null - hue) * delta * 7.5;

	amm_mp8 = clamp(amm_mp8, 0, max_mp8)
	amm_grenade[0] = clamp(amm_grenade[0], 0, max_grenade[0])
	amm_grenade[1] = clamp(amm_grenade[1], 0, max_grenade[1])
	amm_energy = clamp(amm_energy, 0, max_energy)





func _input(event):
	if (reloading == 0 && cooldown == 0 && !melee):
		if (sel_weapon == WP_MP8):
			if (event.is_action_pressed("shoot") && sel_rof == FS_SEMI):
				bullet();
			elif (event.is_action_pressed("secondary_shoot")):
				sel_rof += 1;
				if (sel_rof > FS_AUTO):
					sel_rof = FS_SEMI;
		if (event.is_action_pressed("weap_prev")):
			sel_lastweapon = sel_weapon;
			var d = true
			while (d):
				sel_weapon -= 1;
				if (sel_weapon < WP_PLASMA):
					sel_weapon = WP_JOLT;
				if (weapons[sel_weapon]):
					d = false;

		elif (event.is_action_pressed("weap_next")):
			sel_lastweapon = sel_weapon;
			var d = true
			while (d):
				sel_weapon += 1;
				if (sel_weapon > WP_JOLT):
					sel_weapon = WP_PLASMA;
				if (weapons[sel_weapon]):
					d = false;
		elif (event.is_action_pressed("weap_last")):
			var temp = sel_weapon;
			sel_weapon = sel_lastweapon;
			sel_lastweapon = temp;
	if (reloading == 0 && weapons != [false,false,false,false] && event.is_action_pressed("mode")):
		melee = !melee;
		if (sel_weapon == NULL):
			while (!weapons[sel_weapon]):
				sel_weapon += 1;
				if (sel_weapon > WP_JOLT):
					sel_weapon = WP_PLASMA;
























func save_game(slot):
	var file = File.new()
	file.open("user://s" + slot + ".sav", File.WRITE)
	var data = {
		"health" : health
	}
	file.store_line(to_json(data))
	file.close()

func load_game(slot):
	var file = File.new()
	if not file.file_exists("user://s" + slot + ".sav"):
		return

	file.open("user://s" + slot + ".sav", File.READ)
	while not file.eof_reached():
		var current_line = parse_json(file.get_line())

		for i in current_line.keys():
			self.set(i, current_line[i])
	file.close()