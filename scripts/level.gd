extends Spatial

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	if (Input.is_action_pressed("debug_quit")):
		get_tree().quit();
	if (Input.is_action_pressed("debug_reload")):
		get_tree().reload_current_scene()
		game.health = 100
		game.ded = false
#		game.amm_mp8 = 48
#		game.amm_grenade = [3, 2]
#		game.amm_energy = 50

	pass