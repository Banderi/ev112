extends KinematicBody

onready var sprite = get_node("sprite")
onready var coll = get_node("collision")
onready var shadow = get_node("shadow")

###

const NULL = -1



const WP_PLASMA = 0
const WP_MP8 = 1
const WP_GRENADE = 2
const WP_JOLT = 3

export(int, \
	"Plasma Gun", \
	"MP8", \
	"Grenade Gun", \
	"Jolt Gun", \
	"other") var item = NULL
export var quantity = 1
export var ammo = 1
export(int, "Normal", "Electric") var ammotype = 0



###

var gravity = -30
var gravity_v = Vector3(0, gravity, 0)
var up = Vector3(0, 1, 0)

var v_linear = Vector3(0, 10, 0)

var t = 0
var touch = false

func _ready():
	if (item == 0):
		sprite.texture = load("res://graphics/obj_plasma.png")
	elif (item == 1):
		sprite.texture = load("res://graphics/obj_mp8.png")
	elif (item == 2):
		sprite.texture = load("res://graphics/obj_grenadegun.png")
	elif (item == 3):
		sprite.texture = load("res://graphics/obj_jolt.png")

func _on_touch(body):
	if (body.get_name() != "player"):
		return;
	touch = true;

func _on_leave(body):
	if (body.get_name() != "player"):
		return;
	touch = false;

func _physics_process(delta):
	t += delta * 2;

	sprite.translation = coll.translation + Vector3(0, sin(t) * 0.1 + 0.25, 0);
	shadow.scale = Vector3(1,1,1) * (0.80 + sin(-t) * 0.10);

	v_linear = move_and_slide(v_linear + gravity_v * delta, up);

	if (touch):
		# attempt pickup and get response 'r'
		var r = game.pickup({
			"item" : item,
			"quantity" : quantity,
			"ammo" : ammo,
			"ammotype" : ammotype
		});
		if (r <= 0 && r != -1):
			queue_free();
		elif (ammo != r && r != -1):
			ammo = r;