extends Polygon2D

var rot = 0.0


# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _process(delta):

	texture_offset = get_position() / get_scale() + offset.rotated(rot)
	texture_scale = get_scale()

	set_rotation(rot)
	set_texture_rotation(rot)