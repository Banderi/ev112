
extends Camera

# Member variables
var collision_exception = []
export var min_distance = 0.5
export var max_distance = 4.0
export var angle_v_adjust = 0.0
export var autoturn_ray_aperture = 25
export var autoturn_speed = 50
var max_height = 2.0
var min_height = 0
var up = Vector3(0, 1, 0)
var displacement = Vector3(0, 6, 15);
var looker = Vector3(0, 0, 0);

func _physics_process(delta):

	looker += (Vector3(2.5,0,0) * Input.get_joy_axis(0, JOY_AXIS_2) + Vector3(0,-2.5,0) * Input.get_joy_axis(0, JOY_AXIS_3) - looker) * delta * 2;


	var target = get_parent().get_global_transform().origin + looker;
	var pos = get_global_transform().origin
	var col = get_node("camera-col")
	var colpos = col.get_global_transform().origin

	###

	var idealpos = target + displacement;
	var diff = idealpos - pos;
	var newpos = pos + diff * delta * 4;
	var delay = pos - newpos;
	var newtarget = target + delay;
	var collstep = newpos - colpos;

	col.move_and_slide((idealpos - colpos) * 3, up);
#	col.move_and_collide(collstep);

	look_at_from_position(colpos, newtarget, up)

	# Turn a little up or down
	var t = get_transform()
	t.basis = Basis(t.basis[0], deg2rad(angle_v_adjust)) * t.basis
	set_transform(t)


func _ready():
	# Find collision exceptions for ray
	var node = self
	while(node):
		if (node is RigidBody):
			collision_exception.append(node.get_rid())
			break
		else:
			node = node.get_parent()
	set_physics_process(true)
	# This detaches the camera transform from the parent spatial node
#	set_as_toplevel(true)






#var target = get_parent().get_global_transform().origin
#	var pos = get_global_transform().origin
#	var col = get_node("../../camera-col")
#	var colpos = col.get_global_transform().origin

#	var dist = (target - pos)

#	# Check ranges
#	if (dist.length() < min_distance):
#		dist = dist.normalized()*min_distance
#	elif (dist.length() > max_distance):
#		dist = dist.normalized()*max_distance
#
#	# Check upper and lower height
#	if (dist.y > max_height):
#		dist.y = max_height
#	if (dist.y < min_height):
#		dist.y = min_height
#
#	# Check autoturn
#	var ds = PhysicsServer.space_get_direct_state(get_world().get_space())

#	var space_state = get_world().direct_space_state;
#
#	var col_left = space_state.intersect_ray(target, target + Basis(up, deg2rad(autoturn_ray_aperture)).xform(dist), collision_exception)
#	var col = space_state.intersect_ray(target, target + dist, collision_exception)
#	var col_right = space_state.intersect_ray(target, target + Basis(up, deg2rad(-autoturn_ray_aperture)).xform(dist), collision_exception)
##
#	if (!col.empty()):
#		# If main ray was occluded, get camera closer, this is the worst case scenario
#		dist = col.position - target
#	elif (!col_left.empty() and col_right.empty()):
#		# If only left ray is occluded, turn the camera around to the right
#		dist = Basis(up, deg2rad(-displacement * autoturn_speed)).xform(dist)
#	elif (col_left.empty() and !col_right.empty()):
#		# If only right ray is occluded, turn the camera around to the left
#		dist = Basis(up, deg2rad(delta * autoturn_speed)).xform(dist)
#	else:
#		# Do nothing otherwise, left and right are occluded but center is not, so do not autoturn
#		pass
##
##	# Apply lookat
##	if (dist == Vector3()):
##		dist = (pos - target).normalized()*0.0001

#	pos = colpos;

#	var cinv = get_parent().get_global_transform().inverse()
#	col.set_transform(cinv)





#	if(pos.distance_squared_to(newpos) < 0.0001):
#		newpos = idealpos;

#	colpos = idealpos;

#	col.set_axis_velocity((newpos - colpos) * 0.1);
#	col.move_and_collide((idealpos - colpos) * delta * 0.01);
#	col.move_and_collide((newpos - colpos) - delay);
#	col.move_and_slide(Vector3(), up);

##	colpos = col.get_global_transform().origin;
#	look_at_from_position(pos, newtarget, up);
#
#	# Turn a little up or down
#	var t = get_transform()
#	t.basis = Basis(t.basis[0], deg2rad(angle_v_adjust)) * t.basis
#	set_transform(t)