
extends KinematicBody

#onready var sprite = get_node("target/sprite")
#onready var sprite_outl = get_node("target/sprite_outl")
#onready var viewport = get_node("viewport_post")
#onready var post = get_node("post")
onready var mesh = get_node("vanix")
onready var camera = get_node("target/camera")
onready var animset = mesh.get_node("AnimationPlayer")
onready var im = get_node("debug")

var current_anim = "02_idle"

var facing_dir = 2

var jumping = false
var falling = false
var sprinting = false
var crouching = false
var stance = false
var climbing = false
var sliding = false
var onground = true
var footing = true
var friction = true
var clinging = false
var walls = false

var wasonground = true
var didjump = false
var splat = false

var turn_friction = 0.2
var accel = 19.0
var deaccel = 14.0
var sharp_turn_threshold = 140

var gravity = -19.8
var gravity_v = Vector3(0, gravity, 0)
var up = Vector3(0, 1, 0)

var v_linear = Vector3()
var v_ground = Vector3()
var v_falling = Vector3()
var mov = Vector3()
var oldmov = Vector3()
var rot = 0

var tick = 0

###

func setAnimation(anim, speed):
	if (anim == "null"):	anim = "00_NULL";
	if (anim == "test"):	anim = "01_test";
	if (anim == "idle"):	anim = "02_idle";
	if (anim == "walk"):	anim = "03_walk";
	if (anim == "run"):		anim = "04_run";
	if (anim == "sprint"):	anim = "05_sprint";
	if (anim == "crouch"):	anim = "06_crouch";
	if (anim == "sneak"):	anim = "07_sneak";
	if (anim == "roll"):	anim = "08_roll";
	if (anim == "stance"):	anim = "09_stance";
	if (anim == "jump"):	anim = "10_jump";
	if (anim == "fall"):	anim = "11_fall";
	if (anim == "land"):	anim = "12_land";
	if (anim == "slide"):	anim = "13_slide";
	if (anim == "climb"):	anim = "14_climb";
	if (anim == "hland"):	anim = "15_hland";
	if (anim == "death"):	anim = "16_death";
	if (anim == "ded"):		anim = "17_ded";

	animset.playback_speed = speed;

	if (current_anim != anim):
		animset.play(anim);
		current_anim = animset.current_animation;

func _physics_process(delta):
	if (game.ded):
		setAnimation("death", 1.0);
		return;
	if (splat):
		if (animset.current_animation != "15_hland"):
			splat = false;
		return;
	if (sprinting && onground && !sliding && mov != Vector3()):
		game.stamina -= delta * 25;
	elif (game.stamina < 100):
		if (stance):
			game.stamina += delta * 10;
		else:
			game.stamina += delta * 25;

	sprinting = false;
	crouching = false;
	stance = false;
	climbing = false;
	sliding = false;
	onground = false;
	footing = false;
	friction = false;
	clinging = false;
	walls = false;

	current_anim = animset.current_animation;

	facing_dir = 0;

	if (mov != Vector3()):
		oldmov = mov;

	var pos = get_global_transform().origin;
	var space_state = get_world().direct_space_state;

	var v_vert = up.dot(v_linear);
	var v_horz = v_linear - up * v_vert;

	###

	var phi = camera.get_rotation().y;
	var kmov = Vector3();
	var xmov = Vector3();

	if (Input.is_action_pressed("move_up")):
		facing_dir += 3;
		kmov += Vector3(0, 0, -1);
	if (Input.is_action_pressed("move_down")):
		facing_dir -= 3;
		kmov += Vector3(0, 0, 1);
	if (Input.is_action_pressed("move_left")):
		facing_dir += 2;
		kmov += Vector3(1, 0, 0);
	if (Input.is_action_pressed("move_right")):
		facing_dir -= 2;
		kmov += Vector3(-1, 0, 0);

	xmov = Vector3(
		sin(phi) * Input.get_joy_axis(0, JOY_AXIS_1) + cos(phi) * Input.get_joy_axis(0, JOY_AXIS_0),
		0,
		sin(phi) * Input.get_joy_axis(0, JOY_AXIS_0) + cos(phi) * Input.get_joy_axis(0, JOY_AXIS_1));
	if (xmov.length() < 0.2):
		xmov = Vector3();

	var mov_flat = kmov + xmov;
	if (mov_flat.length() > 1):
		mov_flat = mov_flat.normalized();
	mov_flat = mov_flat.rotated(up, phi);

	# tilt movement following slopes
	var m = Basis();
	var raypos = pos + up;
	var rayend = pos - up;
	if (v_vert < -1):
		rayend = pos - up * abs(v_vert);
	var result = space_state.intersect_ray(raypos, rayend);
	var normal = up;
	mov = mov_flat;
	if (result.has("normal") && result.normal != up):
		normal = result.normal;
		var c = up.cross(normal);
		var a = acos(up.dot(normal));
		m = m.rotated(c.normalized(), a);
		mov = m.xform(mov_flat);
	var mov_speed = mov.length();

	###

	var inters = Vector3();
	if (result.has("position")):
		inters = result.position;
#	var dist_sq = inters.distance_squared_to(pos);
	var dist = inters.distance_to(pos);
	var angle = normal.angle_to(up);

	var col_normal = normal;
	if (get_slide_count() > 0):
		var coll = get_slide_collision(get_slide_count() - 1);
		col_normal = coll.normal;
	var col_angle = col_normal.angle_to(up);

	###

	if (is_on_wall()):
		if (col_angle <= 1.56):
			onground = true;
		else:
			walls = true;
#			if (abs(v_linear.y) < 1):
#				clinging = true;
	if (is_on_floor()):
		onground = true;
		footing = true;
	elif (onground):
		sliding = true;
	elif (v_vert < 0):
		jumping = false;
		falling = true;

	if (footing):
		friction = true;

	if (!onground && !jumping && wasonground && !sliding && !crouching && dist < abs(v_linear.y)): # fix slope/staircase jitter
		mov.y = -10;
		onground = true;
		falling = false;

	if (onground):
		if (falling || jumping):
			if (v_falling.y < -15):
				game.health -= pow(v_falling.y + 13.5, 2) * 0.35;
				if (footing):
					setAnimation("hland", 2.75);
					splat = true;
			else:
				setAnimation("land", 3.0);
		v_ground = v_linear;
		falling = false;
		jumping = false;
	elif (v_linear != Vector3()):
		v_falling = v_linear;

	###

	var jump_attempt = Input.is_action_pressed("jump");

	if (Input.is_action_pressed("crouch")):
		crouching = true;
#		if (onground && !footing && col_angle < 1 && v_linear.length() < 3): # climbing on ripid slopes
#			sliding = false;
#			climbing = true;
#			friction = true;

	if (Input.is_action_pressed("sprint") && !crouching && !game.fatigue):
		sprinting = true;

	if (Input.is_action_pressed("stance") && mov == Vector3() && footing):
		stance = true;

	###

	var speed = 6;
	var maxspeed = 0;
	var allowance = 6;
	var jump_force = 0;
	var jump_v = Vector3();

	if (footing):
		turn_friction = 0.2;
		speed = 6;
		if (sprinting):
			speed = 12;
			if (mov_speed > 0):
				mov = mov.normalized();
				mov_speed = 1;
		elif (crouching):
			speed = 2;
	elif (climbing):
		speed = 2;
	elif (sliding):
		speed = 1;
	else:
		speed = 0.3;

	if (!onground):
		turn_friction = 0.203;
		mov = mov_flat; # ignore slope correction while freefalling
	elif (sliding && !jumping):
		turn_friction = 0.4;
		mov = Vector3();
#		mov /= 10 * abs(mov.y) + 1; # can only move trasversally while sliding off a surface
		if (v_linear.y > 0):
			v_linear *= 0.99; # fix slope walking - p.s.: it doesn't fix shit

	if (jump_attempt && !jumping && !didjump):
		jumping = true;
		if (footing && !crouching): # liftoff! JUMP!
			jump_force = 7.5;
			game.stamina -= 20;
			setAnimation("jump", 1.75);
			if (stance):
				jump_force = 14;
				game.stamina -= 30;
				setAnimation("jump", 1.0);
		elif (sliding && v_linear.y < 0):
			v_linear *= 0.5;
			mov = Vector3();
			game.stamina -= 20;
			jump_v = col_normal * (10) + up * 1.5; # jumping off of ripid slopes
		elif (clinging && dist > 2.0):
			jump_v = col_normal * (10) + up * 6.9; # wall-jumping
		else:
			jumping = false; # actually no jump after all... ;_;
		if (jumping):
			didjump = true;
	if(!jump_attempt && onground):
		didjump = false;

	var speedmov = mov * speed; # movement vector times speed
	var defacto = v_horz + speedmov; # expected movement displacement

	###

	result = space_state.intersect_ray(mov * 0.5 + raypos, mov * 0.5 + rayend);
	var normal_n = up;
	if (result.has("normal") && result.normal != up):
		normal_n = result.normal;
	var inters_n = Vector3();
	if (result.has("position")):
		inters_n = result.position;
	var angle_n = normal_n.angle_to(up);
	var angle_o = normal_n.angle_to((mov_flat).normalized());
	if (angle_o > PI * 0.5 && !jumping && ((!crouching && angle_n > PI * 0.25) || (crouching && angle_n > PI * 0.25))): # for climbing, max angle was 1
		speedmov = Vector3(); # fix walking on ripid slopes
#	im.clear()
#	im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
#	im.set_color(Color(1, 0, 0))
#	im.add_vertex(inters_n + normal_n * 2 - pos)
#	im.add_vertex(inters_n - pos)
#	im.end()

	###

	if (jump_force != 0):
		jump_v = up * jump_force + v_ground - up * v_ground;
	var final = speedmov + jump_v + gravity_v * delta;
	if (clinging):
#		v_linear *= 0.99;
#		if (v_linear.length() < 1.0):
#			v_linear = Vector3();
		if (final.y < -0.1):
			final.y = -0.1;
#	var final_h = (final - up * final);
#	var expected_h = v_horz + final_h;
#	var is_accel = (expected_h.length() > v_horz.length());

	v_ground.y = 0.0;
	maxspeed = max(v_ground.length(), 6);


	if (!friction):
		final = v_linear + final;
		var final_h = (final - up * final);
		if (final_h.length() > maxspeed):
			final_h = final_h.normalized() * maxspeed;
			final = Vector3(final_h.x, final.y, final_h.z);
#	else:
#		final = v_linear * 0.4 + final;

	v_linear = move_and_slide(final, up);


#		v_linear = move_and_slide(final, up);
##	elif (!friction && is_accel && expected_h.length() > maxspeed):
###
###		var ratio = maxspeed / expected_h.length();
###		var corrected = (v_linear + final) * ratio;
###		corrected.y = (v_linear + final).y;
##		v_linear = move_and_slide(v_linear + final - final_h, up);
###		v_linear = move_and_slide(corrected, up);
#	else:
#

	###

	var newr = 0;
	var v_flat = v_linear; v_flat.y = 0;
	var min_vel = 1.0;
	if (sliding || (!onground && v_flat.length() > min_vel)):
		newr = v_linear.angle_to(Vector3(1, 0, 0));
		if (v_linear.z > 0):
			newr = PI * 2 - newr;
	elif (mov_flat):
		newr = mov_flat.angle_to(Vector3(1, 0, 0));
		if (mov_flat.z > 0):
			newr = PI * 2 - newr;
	if (mov_flat || sliding || (!onground && v_flat.length() > min_vel)):
		while (newr - rot > PI):
			newr -= PI * 2;
		while (newr - rot < -PI):
			newr += PI * 2;

		rot += (newr - rot) * turn_friction;
		while (rot > PI * 2):
			rot -= PI * 2;
		while (rot < -PI * 2):
			rot += PI * 2;

	var scs = 1;
	var ax = up.cross(normal).normalized();
	var a = col_angle;
	if (ax == Vector3() || (!sliding && !climbing) || current_anim == "13_slip" || angle <= PI * 0.25):
		ax = up;
		a = 0;
	mesh.set_transform(Transform(Transform(Basis()).rotated(up, rot + PI * 0.5).rotated(ax, a).scaled(Vector3(scs, scs, scs)).basis,Vector3(0,0.03,0)));

	###

#	var sprite = get_node("sprite_test");
#
#	if (jumping):
#		sprite.set_animation("jump");
#	elif (falling):
#		sprite.set_animation("fall");
#	elif (sliding):
#		sprite.set_animation("sliding");
#	elif (crouching):
#		sprite.set_animation("crouch");
#	elif (stance):
#		sprite.set_animation("stance");
#	else:
#		if (mov.length() > 0):
#			sprite.set_animation("run");
#		else:
#			sprite.set_animation("idle");
#	if (oldmov.x < 0):
#		sprite.flip_h = true;
#	else:
#		sprite.flip_h = false;

#	viewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME);
#	sprite.texture = viewport.get_texture();
#	sprite_outl.texture = viewport.get_texture();

#	viewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME);
#	post.texture = viewport.get_texture();

	###

	if (splat):
		return;

	animset.playback_default_blend_time = 0.1;
	if (jumping):
		setAnimation("jump", 1.0);
	elif (onground):
		if (sliding && angle > PI * 0.25):
			if (current_anim != "13_slide"):
				animset.playback_default_blend_time = 0.0;
			setAnimation("slide", 3.0);
		elif (climbing):
			if (mov_speed > 0):
				setAnimation("climb", 1.6);
			else:
				setAnimation("climb", 0.0);
		elif (crouching):
			if (mov_speed > 0):
				setAnimation("sneak", 1.6 * mov_speed);
			else:
				setAnimation("crouch", 1.0);
		elif (stance):
			setAnimation("stance", 1.0);
		elif (mov_speed > 0):
			if (sprinting):
				setAnimation("sprint", 2.5 * mov_speed);
			else:
				setAnimation("run", 1.8 * mov_speed);
		elif (current_anim != "12_land" && current_anim != "15_hland"):
			animset.playback_speed = 1.0;
			setAnimation("idle", 1.0);
	elif (falling && !jumping && current_anim != "10_jump"):
		setAnimation("fall", 2.0);


	###

	im.get_node("fspeed").text = str(game.cooldown);

	var mat = mesh.get_node("Armature/Skeleton/mesh").get_surface_material(0);
	mat.next_pass.set_shader_param("flashcolor", game.hue);

	var debug = false;

	if (debug):


		im.clear()

		im.begin(Mesh.PRIMITIVE_POINTS, null)
		im.set_color(Color(0, 0, 0))
		if (jumping):
			im.set_color(Color(0, 0, 1))
		elif (falling):
			im.set_color(Color(1, 0, 0))
		elif (climbing):
			im.set_color(Color(1, 1, 0))
		elif (sliding):
			im.set_color(Color(1, 0.5, 0))
		elif (crouching):
			im.set_color(Color(0, 0.3, 0))
		elif (sprinting):
			im.set_color(Color(0, 0.6, 0.5))
		elif (stance):
			im.set_color(Color(1, 0, 1))
		elif (mov != Vector3()):
			im.set_color(Color(0, 1, 0))
		im.add_vertex(Vector3(0, 3, 0))
		im.add_vertex(Vector3(0, 3.1, 0))
		im.end()

		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		im.set_color(Color(1, 1, 0))
		im.add_vertex(Vector3())
		im.add_vertex(v_linear * 0.5)
		im.end()

		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		im.set_color(Color(0.6, 0, 0, 0.5))
		im.add_vertex(Vector3())
		im.add_vertex(mov_flat)
		im.end()

		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		im.set_color(Color(1, 0, 1))
		im.add_vertex(Vector3())
		im.add_vertex(mov)
		im.end()

		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		im.set_color(Color(0, 1, 0))
		im.add_vertex(raypos - pos)
		im.add_vertex(rayend - pos)
		im.end()

		if (inters != Vector3()):
			im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
			im.set_color(Color(0, 0, 1))
			im.add_vertex(inters - pos)
			im.add_vertex(inters - pos + normal)
			im.end()

			im.begin(Mesh.PRIMITIVE_POINTS, null)
			im.set_color(Color(0, 0, 1))
			im.add_vertex(inters - pos)
			im.add_vertex(inters - pos + Vector3(0, 0.1, 0))
			im.end()


	###

	wasonground = onground;
	if (sliding || crouching):
		wasonground = false;
	tick += delta;

func _ready():
#	var mesh = get_node("vanix");
#	var animset = mesh.get_node("AnimationPlayer");

	animset.playback_default_blend_time = 0.1;

	animset.animation_set_next("02_idle","02_idle");
	animset.animation_set_next("03_walk","03_walk");
	animset.animation_set_next("04_run","04_run");
	animset.animation_set_next("05_sprint","05_sprint");
	animset.animation_set_next("06_crouch","06_crouch");
	animset.animation_set_next("07_sneak","07_sneak");
	animset.animation_set_next("08_roll","06_crouch");
	animset.animation_set_next("09_stance","09_stance");
	animset.animation_set_next("10_jump","11_fall");
	animset.animation_set_next("11_fall","11_fall");
	animset.animation_set_next("12_land","02_idle");
	animset.animation_set_next("13_slide","13_slide");
	animset.animation_set_next("14_climb","14_climb");
	animset.animation_set_next("15_hland","02_idle");
	animset.animation_set_next("16_death","17_ded");
	animset.animation_set_next("17_ded","17_ded");

	animset.play("02_idle");