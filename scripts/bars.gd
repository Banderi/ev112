extends Node2D

var t = 0
var r = 0

onready var player = self.owner

onready var hp = get_node("bar_health")
onready var stamina = get_node("bar_stamina")
onready var skill_0 = get_node("bar_skill0")
onready var skill_1 = get_node("bar_skill1")
onready var ammo = get_node("bar_ammo")

onready var weapon = get_node("../weapon")
onready var gauge = get_node("../gauge")
onready var gaugeneedle = get_node("../gauge/needle")
onready var ammolabel = get_node("../txt_ammo")
#onready var ammotex_null = ammo.texture
onready var ammotex_bullet = load("res://graphics/hud_ammo_bullet.png")
onready var ammotex_pellet = load("res://graphics/hud_ammo_pellet.png")
onready var ammotex_energy = load("res://graphics/hud_ammo_energy.png")

onready var wp_plasma = load("res://graphics/wp_plasma.png")
onready var wp_mp8 = load("res://graphics/wp_mp8.png")
onready var wp_grenade = load("res://graphics/wp_grenade.png")
onready var wp_jolt = load("res://graphics/wp_jolt.png")

var ammotex = game.NULL

func _ready():
	assert(player != null)
	pass

func _process(delta):

	hp.rot = game.health/100 * (1.3 + 0.47) - 0.47
	stamina.rot = (100-game.stamina)/100 * (2.56 - 1.1) + 1.1
	skill_0.rot = min(50,game.skill)/50 * PI
	skill_1.rot = max(50,game.skill)/50 * PI


	if (game.sel_weapon == game.WP_PLASMA):
		weapon.texture = wp_plasma
		ammo.texture = ammotex_pellet
		ammolabel.text = "unl"
		ammo.rot = float(game.mag_plasma)/game.clp_plasma * -0.41 + 0.002*game.mag_plasma -0.002
		if (game.reloading != 0):
			ammo.rot = (game.rel_plasma - game.reloading)/game.rel_plasma * -0.41

	elif (game.sel_weapon == game.WP_MP8):
		weapon.texture = wp_mp8
		ammo.texture = ammotex_bullet
		if (game.sel_rof == game.FS_AUTO):
			ammolabel.text = '"'
		elif (game.sel_rof == game.FS_SEMI):
			ammolabel.text = "'"
		ammolabel.text += str(int(game.amm_mp8)).pad_zeros(2)
		ammo.rot = float(game.mag_mp8)/game.clp_mp8 * -0.41 + 0.002*game.mag_mp8 -0.002
		if (game.reloading != 0):
			ammo.rot = (game.rel_mp8 - game.reloading)/game.rel_mp8 * -0.41

	elif (game.sel_weapon == game.WP_GRENADE):
		weapon.texture = wp_grenade
		ammo.texture = ammotex_bullet
#		ammolabel.text = "/" + str(int(game.amm_grenade[game.sel_ammo])).pad_zeros(2)
#		ammo.rot = float(game.amm_grenade[game.sel_ammo])/float(game.max_grenade[game.sel_ammo]) * -0.41

		ammolabel.text = str(int(game.amm_grenade[0])) + "/" + str(int(game.amm_grenade[1]))
		ammo.rot = \
			float(game.amm_grenade[0] + game.amm_grenade[1]) / \
			float(game.max_grenade[0] + game.max_grenade[1]) * -0.41 \
			 + 0.002*(game.amm_grenade[0] + game.amm_grenade[1]) -0.002
	elif (game.sel_weapon == game.WP_JOLT):
		weapon.texture = wp_jolt
		ammo.texture = ammotex_energy
		ammolabel.text = str(int(game.amm_energy)).pad_zeros(3)
		ammo.rot = float(game.amm_energy)/game.max_energy * -0.41

	if (game.sel_weapon == game.NULL || game.melee):
		weapon.modulate = Color(1,1,1,0.5)
		ammo.texture = ammotex_energy
		ammolabel.text = "---"
		ammo.rot = 0
	else:
		weapon.modulate = Color(1,1,1,1)

	t += delta
	if (t > PI):
		t -= PI

	var spr = 2
	r += (randf() * spr - (spr/2 + r)) * (delta);

	if (game.sel_weapon == game.WP_JOLT && !game.melee):
		gauge.visible = true;

		var c = 0
		var r2 = 0
		if (game.energy_buildup > 0):
			c = 42 - 42 / (0.2 * (game.energy_buildup) + 1) # 42 is the max at 0.15 speed; 37 is in red zone with spread
			gaugeneedle.set_rotation(-128 + (1 + r) * c * 0.15)
		elif (game.cooldown > 0):
			c = 42 - 42 / (0.2 * (game.cooldown) + 1)
			r2 = abs(sin(t*10)) * 0.0005 * c * c - r
			c = game.cooldown
		elif (game.energy_jolt > 0):
			r2 = abs(sin(t*8)) * 0.1
			c = game.energy_jolt
		gaugeneedle.set_rotation(-128 + (1 + r + r2) * c * 0.15)
	else:
		gauge.visible = false;

	if (game.health < 30):
		hp.color = Color(1,1,1,abs(sin(t*8)))
	else:
		hp.color = Color(1,1,1,1)
	if (game.fatigue):
		stamina.color = Color(1,1,1,abs(sin(t*4)))
	else:
		stamina.color = Color(1,1,1,1)

#	player.health += delta * 100; if (player.health > 100): player.health = 0
#	player.stamina += delta * 50; if (player.stamina > 100): player.stamina = 0
#	player.skill += delta * 30; if (player.skill > 100): player.skill = 0
#	player.ammo += delta * 50; if (player.ammo > 99): player.ammo = 0